import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  host: {
    '(window:resize)': 'onResize($event)'
  }
})
export class AppComponent {

  openSidebar : boolean = true;

  constructor(
    translation : TranslateService
  ) {
    translation.setDefaultLang('en');
    translation.use('en');

    if (window.innerWidth <= 600) {
      this.openSidebar = false;
    } else {
      this.openSidebar = true;
    }
  }

  sidebarOpen() {
    return this.openSidebar;
  }

  toggleSidebar() {
    this.openSidebar = ! this.openSidebar;
  }

  onResize(event) {
    if (event.target.innerWidth <= 600) {
      this.openSidebar = false;
    } else {
      this.openSidebar = true;
    }
  }
}
