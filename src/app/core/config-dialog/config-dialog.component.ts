import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

import { ConfigItem } from './../../core/models/config-item';

@Component({
  selector: 'app-config-dialog',
  templateUrl: './config-dialog.component.html',
  styleUrls: ['./config-dialog.component.css']
})
export class ConfigDialogComponent implements OnInit {

  public configItems : ConfigItem[] = new Array();

  constructor(
    private dialogRef : MatDialogRef<ConfigDialogComponent>
  ) { 
  }

  ngOnInit() {
  }

  save() {
    this.dialogRef.close(this.configItems);
  }

}
