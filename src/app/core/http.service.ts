import { Injectable } from '@angular/core';
import { Http, XHRBackend, RequestOptions, Request, RequestOptionsArgs, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class HttpService extends Http {

  constructor(
    backend : XHRBackend,
    options : RequestOptions
  ) { 
    let token = localStorage.getItem( 'auth_token' );
    options.headers.set( 'Authorization', `Bearer ${token}` );
    options.headers.set( 'Content-Type', 'application/json' );

    super( backend, options );
  }

  request( url : string|Request, options? : RequestOptionsArgs ) : Observable< Response > {
    let token = localStorage.getItem( 'auth_token' );
    if( typeof url === 'string' ) {
      if( ! options ) {
        options = { headers: new Headers() };
      }

      options.headers.set( 'Authorization', `Bearer ${token}` );
      options.headers.set( 'Content-Type', 'application/json' );
    } else {
      url.headers.set( 'Authorization', `Bearer ${token}` );
      url.headers.set( 'Content-Type', 'application/json' );
    }                                   

    return super.request( url, options ).catch( this.catchAuthError( this ) );
  }

  private catchAuthError( self : HttpService ) {
    return ( res : Response ) => {
      return Observable.throw( res );
    };
  }

}