import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog, MatDialogConfig } from '@angular/material';
import { Observable } from 'rxjs';

import { ConfirmDialogComponent }  from './confirm-dialog/confirm-dialog.component';
import { LoadingDialogComponent } from './loading-dialog/loading-dialog.component';
import { ConfigDialogComponent } from './config-dialog/config-dialog.component';

import { ConfigItem } from './models/config-item';

@Injectable()
export class DialogService {

  constructor(
    private dialog : MatDialog
  ) { }

  public confirm(title: string, message: string) : Observable<Response> {
    let dialogRef : MatDialogRef<ConfirmDialogComponent>;

    dialogRef = this.dialog.open(ConfirmDialogComponent);
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.message = message;

    return dialogRef.afterClosed();
  }

  public loading() : MatDialogRef<LoadingDialogComponent> {
    let dialogRef : MatDialogRef<LoadingDialogComponent>;

    dialogRef = this.dialog.open(LoadingDialogComponent);

    return dialogRef;
  }

  public config(configItems: ConfigItem[]) : Observable<ConfigItem[]> {
    let dialogRef : MatDialogRef<ConfigDialogComponent>;

    dialogRef = this.dialog.open(ConfigDialogComponent);
    dialogRef.componentInstance.configItems = configItems;

    return dialogRef.afterClosed();
  }

}
