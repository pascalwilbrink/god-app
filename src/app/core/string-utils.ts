interface String {
    format(args) : string;
}

String.prototype.format = function (args) {
    var str = this;
    return str.replace(new RegExp("{-?[0-9]+}", "g"), function(item) {
        var intVal = parseInt(item.substring(1, item.length - 1));
        var replace;
        if (intVal >= 0) {
            replace = args[intVal];
        } else if (intVal === -1) {
            replace = "{";
        } else if (intVal === -2) {
            replace = "}";
        } else {
            replace = "";
        }
        return replace;
    });
};
