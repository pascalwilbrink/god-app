import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from "@angular/common/http";

import { XHRBackend, RequestOptions, HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { FlexLayoutModule } from '@angular/flex-layout';

// Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { HttpService } from './http.service';
import { DialogService } from './dialog.service';
import { ErrorService } from './error.service';

import {
  MatCardModule,
  MatDialogModule,
  MatProgressSpinnerModule,
  MatButtonModule,
  MatListModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
} from '@angular/material';

import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { LoadingDialogComponent } from './loading-dialog/loading-dialog.component';
import { ConfigDialogComponent } from './config-dialog/config-dialog.component';
import { ErrorComponent } from './error/error.component';

export function buildHttpService( backend : XHRBackend, defaultOptions : RequestOptions ) {
  return new HttpService(backend, defaultOptions);    
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    FlexLayoutModule,
    MatCardModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [ConfirmDialogComponent, LoadingDialogComponent, ConfigDialogComponent, ErrorComponent],
  entryComponents: [
    ConfirmDialogComponent,
    LoadingDialogComponent,
    ConfigDialogComponent
  ],
  exports: [ErrorComponent]
})
export class CoreModule { 
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [ 
        {
          name : HttpService,
          provide: HttpService,
          useFactory: buildHttpService,
          deps: [ XHRBackend, RequestOptions ]
          
        },
        DialogService,
        ErrorService
      ]
    };
  }
}
