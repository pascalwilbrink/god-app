export class ConfigItem {

    id : string;
    
    key : string;

    value : string;

    possibleValues : string[] = new Array();
    
}