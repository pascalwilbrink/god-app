import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Subject } from 'rxjs';

@Injectable()
export class ErrorService {

  private _error: Subject<string> = new Subject();
  
  constructor() { }

  clearError() {
    this._error.next();
  }

  setError(error: Response) {
    this._error.next(error.json().message);
  }

  getError() {
    return this._error.asObservable();
  }

}
