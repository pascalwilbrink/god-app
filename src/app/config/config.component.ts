import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.css']
})
export class ConfigComponent implements OnInit {

  lang: string;
  languages: any[] = [
    {
      "code": "en",
      "name": "English"
    }, {
      "code": "nl",
      "name": "Nederlands"
    }
  ];

  constructor(
    private translate: TranslateService
  ) {
    this.lang = this.translate.currentLang;
   }

  ngOnInit() {
  }

  save() {
    this.translate.use(this.lang);
  }

}
