import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CommonModule } from '@angular/common';
import { CoreModule } from './../core/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { LightRoutingModule } from './lights.routes';

import { LightsService } from './lights.service';
import { LightConfigService } from './light-config.service';

import { FlexLayoutModule } from '@angular/flex-layout';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

import { 
  MatCardModule,
  MatListModule,
  MatSlideToggleModule,
  MatSliderModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatButtonModule
} from '@angular/material';

import { LightsComponent } from './lights.component';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    MatCardModule,
    MatListModule,
    MatSlideToggleModule,
    MatSliderModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    FlexLayoutModule,
    LightRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    LightsService,
    LightConfigService
  ],
  declarations: [LightsComponent],
  entryComponents: [
  ]
})
export class LightsModule { }
