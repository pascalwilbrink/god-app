import { TestBed, inject } from '@angular/core/testing';

import { LightConfigService } from './light-config.service';

describe('LightConfigService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LightConfigService]
    });
  });

  it('should be created', inject([LightConfigService], (service: LightConfigService) => {
    expect(service).toBeTruthy();
  }));
});
