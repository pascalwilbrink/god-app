import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { Observable, Observer, Subject, BehaviorSubject } from 'rxjs';
import { forkJoin } from "rxjs/observable/forkJoin";

import { HttpService } from './../core/http.service';

import { ENV } from './../../environments/environment';

import { ConfigItem } from './../core/models/config-item';

@Injectable()
export class LightConfigService {

  constructor(
    private http : HttpService
  ) { }

  getAllConfigItems() : Observable<ConfigItem[]> {
    return this.http.get(ENV.base_urls.light + '/config')
      .map(res => res.json());
  }

  updateConfigItem(configItem : ConfigItem) : Observable<ConfigItem> {
    return this.http.put(ENV.base_urls.light + '/config/' + configItem.id, JSON.stringify(configItem))
      .map(res => res.json());
  }

  updateConfigItems(configItems : ConfigItem[]) : Observable<Response[]> {
    return forkJoin(
      configItems.map(configItem => this.http.put(ENV.base_urls.light + '/config/' + configItem.id, JSON.stringify(configItem)))
    );
  }
}
