import { Component, OnInit } from '@angular/core';

import { LightsService } from './lights.service';
import { LightConfigService } from './light-config.service';
import { Light } from './model/light';

import { DialogService } from './../core/dialog.service';

@Component({
  selector: 'app-lights',
  templateUrl: './lights.component.html',
  styleUrls: ['./lights.component.scss']
})
export class LightsComponent implements OnInit {

  lights : Light[] = new Array();

  selectedLight : Light;

  constructor(
    private lightsService : LightsService,
    private lightConfigService : LightConfigService,
    private dialogService : DialogService
  ) { 
  }

  ngOnInit() {
    this.lightsService.getLights()
      .subscribe((res) => {
        this.lights = res;
        if(this.selectedLight) {
          this.selectedLight = res.filter(light => light.id === this.selectedLight.id)[0];
        } else if(this.lights) {
          this.selectedLight = this.lights[0];
        }
      });
  }

  selectLight(light: Light) {
    this.selectedLight = light;
  }

  changeLight() {
    this.lightsService.updateLightState(this.selectedLight);
  }

  openConfigModal() {
    this.lightConfigService.getAllConfigItems()
      .subscribe((configItems) => {
        this.dialogService.config(configItems)
          .subscribe((updatedConfigItems) => {
            this.lightConfigService.updateConfigItems(updatedConfigItems)
              .subscribe((res) => {
                this.lightsService.load();
              });
          });
      });
  }

}
