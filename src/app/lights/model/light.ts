import { LightState } from './light-state';

export class Light {
    
    id: string;

    name: string;

    state: LightState
    
}

