export class LightState {

    brightness: number;

    hue: number;

    saturation: number;

    on: boolean;

    reachable: boolean;

    transitionTime: number;
}
