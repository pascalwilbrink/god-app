import { Injectable } from '@angular/core';
import { Observable, Observer, Subject, BehaviorSubject } from 'rxjs';

import { ENV } from './../../environments/environment';

import { HttpService } from './../core/http.service';
import { ErrorService } from './../core/error.service';

import { Light } from './model/light';

@Injectable()
export class LightsService {

  private subject;

  private _lights: BehaviorSubject<Light[]> = new BehaviorSubject([]);
      
  constructor(
    private http: HttpService,
    private error: ErrorService
  ) { 
    this.subject = Observable.webSocket(ENV.websockets.light);

    this.subject.subscribe((res) => {
      if(res.source === 'Light') {
        this.load();
      }
    });

    this.load();
  }

  load() {
    this.http.get(ENV.base_urls.light + '/lights')
      .map(res => res.json())
      .subscribe(
        (res) => this._lights.next(res),
        (err) => this.error.setError(err)
      );
  }

  getLights() {
    return this._lights.asObservable();
  }

  getLight(id: string) : Observable< Light > {
    return this.http.get(ENV.base_urls.light + 'lights/' + id)
      .map(res => res.json());
  }

  updateLightState(light: Light) {
    this.http.put(ENV.base_urls.light + '/lights/' + light.id + '/state', JSON.stringify(light.state))
      .subscribe(
        (res) => this.load(),
        (err) => this.error.setError(err)
      );
  }


}

