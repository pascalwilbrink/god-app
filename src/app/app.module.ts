import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { HttpClient, HttpClientModule } from "@angular/common/http";

// Flex layout
import { FlexLayoutModule } from '@angular/flex-layout';

// Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';


// Angular Material
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { 
  MatToolbarModule,
  MatSidenavModule,
  MatCardModule,
  MatListModule,
  MatFormFieldModule,
  MatSelectModule,
  MatButtonModule,
} from '@angular/material';

import { AppComponent } from './app.component';

// Routing
import { AppRoutingModule } from './app.routes';

// Domotica modules
import { CoreModule } from './core/core.module';
import { LightsModule } from './lights/lights.module';
import { ConfigComponent } from './config/config.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    ConfigComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatCardModule,
    MatListModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    CoreModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    AppRoutingModule,
    LightsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
